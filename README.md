# fabric-box-fun

##Startup:
```
yarn install;
yarn start;
```  
##Scripts:
```
"scripts": {
       "start": "parcel serve src/index.html --open",
       "serve": "parcel serve src/index.html",
       "build": "parcel build src/index.html",
       "watch": "parcel watch src/index.html"
 }
```
