console.log('Application Initialized');

import {fabric} from "fabric";

const canvas = new fabric.Canvas("mainCanvas", {
    backgroundColor: "#404448"
});

const checkCollisionWithinBounds = ({checkedObject, referenceObject}) => {

    const collision = {
        occurs: false,
        left: false,
        right: false,
        top: false,
        bottom: false,
        completelyFucked: false
    };

    if (checkedObject.left < referenceObject.left) {
        collision.left = true;
        collision.occurs = true;
    }
    if (checkedObject.right > referenceObject.right) {
        collision.right = true;
        collision.occurs = true;
    }
    if (checkedObject.top < referenceObject.top) {
        collision.top = true;
        collision.occurs = true;
    }
    if (checkedObject.bottom > referenceObject.bottom) {
        collision.bottom = true;
        collision.occurs = true;
    }

    // list edge cases here...
    if (checkedObject.bottom - checkedObject.top > referenceObject.bottom - referenceObject.top ||
        checkedObject.right - checkedObject.left > referenceObject.right - referenceObject.left) {
        collision.occurs = true;
        collision.completelyFucked = true;
    }

    return collision;
};

const OKAY_COLOR = "rgba(0,0,0,0.5)";
const NOPE_COLOR = "rgba(123,0,0,0.5)";
const STROKE_COLOR ="rgba(255,200,50,0.9)";

const DEFAULT_BOUNDING_BOX = {
    width: 400,
    height: 300,
    left: 200,
    top: 50,
    fill: OKAY_COLOR,
    borderColor: STROKE_COLOR,
    cornerColor: STROKE_COLOR,
    hasRotatingPoint: false
};

const cropZoneRectangle = new fabric.Rect();

let lastAction = 'none';

cropZoneRectangle.setOptions({
    ...DEFAULT_BOUNDING_BOX
});

cropZoneRectangle.on({
    scaling: ({/*pointer,*/ target /*, transform*/}) => {
        const {canvasBounds, cropperBounds} = getBoundsInHandyFormat({cropZone: target});
        const collision = checkCollisionWithinBounds({
            checkedObject: cropperBounds,
            referenceObject: canvasBounds
        });
        if (collision.occurs) {
            target.setOptions({
                fill: NOPE_COLOR
            });
        } else {
            target.setOptions({
                fill: OKAY_COLOR
            });
        }
        lastAction = 'scaling';
    },
    moving: ({/*pointer,*/ target /*, transform*/}) => {
        const {canvasBounds, cropperBounds} = getBoundsInHandyFormat({cropZone: target});
        const collision = checkCollisionWithinBounds({
            checkedObject: cropperBounds,
            referenceObject: canvasBounds
        });
        if (collision.occurs) {
            target.setOptions({
                fill: NOPE_COLOR
            });
        } else {
            target.setOptions({
                fill: OKAY_COLOR
            });
        }
        lastAction = 'moving';
    },
    modified: ({/*pointer,*/ target, transform}) => {
        console.log('modified: ', {/*pointer,*/ target, transform});
        onCropZoneModified({cropZone: target});
        lastAction = 'none';
    },
});

const getBoundsInHandyFormat = ({cropZone}) => {
    const canvasBounds = {
        left: 0,
        top: 0,
        right: cropZone.canvas.getWidth(),
        bottom: cropZone.canvas.getHeight(),
        width: cropZone.canvas.getWidth(),
        height: cropZone.canvas.getHeight()
    };

    const cropperBounds = {
        left: cropZone.left,
        top: cropZone.top,
        right: cropZone.left + cropZone.getScaledWidth(),
        bottom: cropZone.top + cropZone.getScaledHeight(),
        width: cropZone.getScaledWidth(),
        height: cropZone.getScaledHeight()
    };

    return {canvasBounds, cropperBounds};
};

const onCropZoneModified = ({cropZone}) => {
    console.log('modified!');

    const {canvasBounds, cropperBounds} = getBoundsInHandyFormat({cropZone});
    const collision = checkCollisionWithinBounds({
        checkedObject: cropperBounds,
        referenceObject: canvasBounds
    });

    const desiredState: any = {...cropperBounds};

    console.log('before: ', {canvasBounds, cropperBounds, collision}, cropZone);
    console.log('cropperBounds:', cropperBounds);

    if (collision.occurs) {

        if (lastAction === 'moving') {
            console.log('it was MOVING!');
            if (collision.left) {
                desiredState.left = canvasBounds.left;
                console.log('left collision. changing left to', desiredState.left);
            }
            if (collision.right) {
                desiredState.left = (canvasBounds.left + canvasBounds.right) - cropperBounds.width;
                console.log('right collision. changing left to', desiredState.left);
            }
            if (collision.top) {
                desiredState.top = canvasBounds.top;
                console.log('top collision. changing top to', desiredState.top);
            }
            if (collision.bottom) {
                desiredState.top = (canvasBounds.top + canvasBounds.bottom) - cropperBounds.height;
                console.log('bottom collision. changing top to', desiredState.top);
            }
        } else if (lastAction === 'scaling') {
            console.log('it was SCALING:');
            if (collision.left) {
                desiredState.width = cropperBounds.right;
                desiredState.left = canvasBounds.left;
                console.log('left collision. changing left to', desiredState.left, ', width to: ', desiredState.width);
            }
            if (collision.right) {
                desiredState.width = canvasBounds.right - cropperBounds.left;
                console.log('right collision. changing width to', desiredState.width);
            }
            if (collision.top) {
                desiredState.height = cropperBounds.bottom;
                desiredState.top = canvasBounds.top;
                console.log('top collision. changing top to', desiredState.top, ', height to: ', desiredState.height);
            }
            if (collision.bottom) {
                desiredState.height = canvasBounds.bottom - cropperBounds.top;
                console.log('bottom collision. changing height to', desiredState.height);
            }
        }

        if (collision.completelyFucked) {
            console.log('Completely fucked. resetting');
            desiredState.width = DEFAULT_BOUNDING_BOX.width;
            desiredState.height = DEFAULT_BOUNDING_BOX.height;
            desiredState.left = DEFAULT_BOUNDING_BOX.left;
            desiredState.top = DEFAULT_BOUNDING_BOX.top;
        }

        desiredState.fill = DEFAULT_BOUNDING_BOX.fill;

        console.log('desired state: ', desiredState);

        cropZone.setOptions(desiredState);
        cropZone.scaleToWidth(desiredState.width);
        cropZone.scaleToHeight(desiredState.height);
        cropZone.setCoords();
        cropZone.saveState();
    }

    console.log('after: ', cropZone.left, cropZone.width, cropZone.top, cropZone.height);
};

canvas.add(cropZoneRectangle);

const canvasBackgroundImage = `https://picsum.photos/800/400/?random`;

fabric.Image.fromURL(
    canvasBackgroundImage,
    (img) => {
        canvas.add(img);
        canvas.sendToBack(img);
        canvas.renderAll();
    },
    {crossOrigin: "Anonymous"}
);
